'use strict';

var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    concat = require('gulp-concat'),
    csso = require('gulp-csso');

var bc = './bower_components/';

gulp.task('js', function () {
    gulp.src('./builds/development/app/**/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./builds/dist/app'))
});


gulp.task('html', function () {
    gulp.src('./builds/development/**/*.html')
        .pipe(gulp.dest('./builds/dist/'))
});

gulp.task('css', function () {
    gulp.src('./builds/development/styles/**/*.css')
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./builds/dist/styles/'))
});

gulp.task('img', function () {
    gulp.src('./builds/development/img/**/*')
        .pipe(gulp.dest('./builds/dist/img/'));
});

gulp.task('fonts', function () {
    gulp.src('./builds/development/fonts/**/*')
        .pipe(gulp.dest('./builds/dist/fonts/'));
});

gulp.task('watch', function () {
    gulp.watch('builds/development/app/**/*.js', ['js']);
    gulp.watch('builds/development/styles/**/*.css', ['css']);
    gulp.watch('builds/development/**/*.html', ['html']);
    gulp.watch('builds/development/img/**/*', ['img']);
    gulp.watch('builds/development/fonts/**/*', ['fonts']);
});


gulp.task('libs', function () {
    gulp.src(bc + 'jquery/dist/jquery.js')
        .pipe(gulp.dest('./builds/dist/libs/jquery/'));

    gulp.src(bc + 'bootstrap/dist/**/*.*')
        .pipe(gulp.dest('./builds/dist/libs/bootstrap/'));

    gulp.src(bc + 'bootstrap-material-design/dist/**/*.*')
        .pipe(gulp.dest('./builds/dist/libs/bootstrap-material-design/'));

    gulp.src([bc + 'angular/angular.js',
        bc + 'angular-aria/angular-aria.js',
        bc + 'angular-animate/angular-animate.js',
        bc + 'angular-cookies/angular-cookies.js',
        bc + 'angular-i18n/angular-locale_ru-ru.js',
        bc + 'angular-loader/angular-loader.js',
        bc + 'angular-resource/angular-resource.js',
        bc + 'angular-route/angular-route.js',
        bc + 'angular-sanitize/angular-sanitize.js',
        bc + 'angular-touch/angular-touch.js',
        bc + 'angular-material/angular-material.js',
        bc + 'angular-bootstrap/ui-bootstrap.min.js',
        bc + 'angular-bootstrap/ui-bootstrap-tpls.min.js',
        bc + 'firebase/firebase.js',
        bc + 'angularfire/dist/angularfire.js',
        "builds/development/script/lib/ng-map.min.js",
        "builds/development/script/lib/jquery.imageReveal.min.js"
    ])
        .pipe(concat('angular.concat.js'))
        .pipe(gulp.dest('./builds/dist/libs/angular/'));
});


gulp.task('webserver', function () {
    gulp.src('./builds/dist/')
        .pipe(webserver({
            livereload: true,
            open: true,
            port: 3000
        }));
});

gulp.task('default', [
    'libs',
    'html',
    'img',
    'js',
    'css',
    'fonts',
    'webserver',
    'watch'
]);