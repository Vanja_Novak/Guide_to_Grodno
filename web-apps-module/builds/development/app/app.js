(function () {
    'use strict';

    angular.module('app', ['ngRoute', 'bootstrapLightbox', 'app.map', 'app.menu', 'app.temp'])
        .config(function ($routeProvider, $locationProvider, $logProvider) {
            $routeProvider.otherwise({
                redirectTo: '/'
            });
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        })
        .constant('CONSTANT', {
            serverURL: 'http://127.0.0.1:8080/api'
        })
})();