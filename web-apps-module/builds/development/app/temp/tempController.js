;(function () {
    "use strict";

    angular.module('app.temp', ['ngRoute', 'ngAnimate', 'ngSanitize', 'ui.bootstrap'])
        .config(['$routeProvider', config])
        .controller('TempCtrl', TempCtrl);

    function config($routeProvider) {
        $routeProvider.when('/temp', {
            templateUrl: 'app/temp/temp.html',
            controller: 'TempCtrl',
            controllerAs: 'vm'
        })
    }

    TempCtrl.$inject = ['$scope', '$rootScope', '$uibModal'];
    function TempCtrl($scope, $rootScope, $uibModal) {
        var slideIndex = 1;
        showDivs(slideIndex);

        $scope.plusDivs = function plusDivs(n) {
            showDivs(slideIndex += n);
        };

        $scope.currentDiv = function currentDiv(n) {
            showDivs(slideIndex = n);
        };

        function showDivs(n) {
            var i;
            var x = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            if (n > x.length) {
                slideIndex = 1
            }
            if (n < 1) {
                slideIndex = x.length
            }
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" w3-white", "");
            }
            x[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " w3-white";
        }
    }
})();