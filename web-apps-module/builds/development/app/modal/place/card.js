angular.module('app.map').controller('PlaceCtrl', function ($scope, $uibModalInstance, item, MapService, Lightbox) {
    var $ctrl = this;

    $ctrl.images = [];
    $ctrl.rating = {
        data: [1, 2, 3, 4, 5]
    };

    $ctrl.newComment = {
        text: null,
        sendComment: sendComment
    };

    if (item !== null) {
        MapService.getPlaceById(item.id)
            .then(function (response) {
                $ctrl.item = response.data;
            });
    } else {
        $ctrl.item = null;
    }


    $ctrl.newComment = {
        text: null,
        rating: 5,
        sendComment: sendComment
    };

    function sendComment() {
        MapService.createCommentPlace({
            entityId: item.id,
            message: $ctrl.newComment.text,
            rating: $ctrl.newComment.rating
        })
            .then(function () {
                    MapService.getPlaceComments(item.id)
                        .then(function (response) {
                            $ctrl.item.comments = response.data;
                        });
                    $ctrl.newComment.text = "";
                    $uibModalInstance.dismiss('cancel');
                }
            );
    }

    $ctrl.cancel = function () {
        $uibModalInstance.dismiss('cancel');

        if ($ctrl.newPlace !== null) {
            $ctrl.newPlace = {};
        }
    };

    $ctrl.openLightboxModal = function (index) {
        Lightbox.openModal($ctrl.item.images, index);
    };

    //Функционал создания марекера.
    if ($ctrl.item === null) {
        //Если нет объекта на просмотр, тогда это режим создания запсии.
        $ctrl.newPlace = {
            title: null,
            name: null,
            groups: null,
            description: null,
            latitude: null,
            longitude: null
        };

        function init() {
            MapService.getAllPlaceGroups()
                .then(function (response) {
                    $ctrl.groups = response.data;
                });
        }

        $ctrl.savePlace = function () {
            if ($scope.placeForm.$valid) {
                MapService.savePlace($ctrl.newPlace);

                $ctrl.cancel();
            }
        };
        init();
    }
});