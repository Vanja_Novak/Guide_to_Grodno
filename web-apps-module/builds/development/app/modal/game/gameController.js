angular.module('app.map')
    .controller('GameCtrl', function ($scope, $uibModalInstance, MapService, Lightbox) {
        var $ctrl = this;

        $ctrl.points = localStorage.getItem('points');
        if ($ctrl.points === null) {
            localStorage.setItem('points', 0);
            $ctrl.points = localStorage.getItem('points');
        }


        $ctrl.listMarkers = [];

        $ctrl.initialize = function () {
            $ctrl.map = new google.maps.Map(document.getElementById('map1'), {
                zoom: 13,
                center: {lng: 23.82649, lat: 53.672030},
                disableDefaultUI: true,
                mapTypeControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL
                }
            });

            google.maps.event.addListener($ctrl.map, "click", function (event) {
                $ctrl.checkParams = {
                    latitude: event.latLng.lng().toFixed(6),
                    longitude: event.latLng.lat().toFixed(6),
                    placeId: $ctrl.item.placeId
                };

                if ($ctrl.responseObject === null || !$ctrl.responseObject.hasGot) {
                    MapService.entrancePointToCircle($ctrl.checkParams)
                        .then(function (res) {
                            $ctrl.responseObject = res.data;

                            if (res.data.hasGot) {
                                var points = localStorage.getItem('points');
                                points = parseInt(points) + parseInt(res.data.points);
                                localStorage.setItem('points', points);
                                $ctrl.points = localStorage.getItem('points');
                            }

                            var iconUrl = res.data.hasGot === true ? '../../img/map/success.png' : null;
                            $ctrl.listMarkers.push(
                                new google.maps.Marker({
                                    position: event.latLng,
                                    animation: res.data.hasGot === true ? google.maps.Animation.BOUNCE : null,
                                    icon: iconUrl,
                                    map: $ctrl.map
                                })
                            );

                        });
                }
            });
        };

        $ctrl.responseObject = {};

        $ctrl.next = function () {
            $ctrl.checkParams = {};
            $ctrl.responseObject = {};

            for (var i = 0; i < $ctrl.listMarkers.length; i++) {
                $ctrl.listMarkers[i].setMap(null);
            }
            $ctrl.listMarkers = [];

            MapService.nextGame()
                .then(function (res) {
                    $ctrl.item = res.data;
                });
        };

        $ctrl.next();

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $ctrl.openLightboxModal = function (index) {
            Lightbox.openModal($ctrl.item.photos, index);
        };

        $(document).ready(function () {
            $(window).resize(function () {
                google.maps.event.trigger($ctrl.map, 'resize');
            });
            google.maps.event.trigger($ctrl.map, 'resize');
        });
    });