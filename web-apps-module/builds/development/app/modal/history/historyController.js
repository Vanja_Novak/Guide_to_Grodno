angular.module('app.map')
    .controller('HistoryCtrl', function ($scope, $uibModalInstance, MapService) {
        var $ctrl = this;

        var notDataFile = "../../img/not_data.jpg";

        $ctrl.item = {
            newPhotoUrl: null,
            oldPhotoUrl: null,
            description: null,
            nextId: null
        };

        function getPhoto(id) {
            MapService.getHistoryPlace(id)
                .then(function (res) {
                    var oldPhotoUrl = res.data.oldPhotoUrl;
                    var newPhotoUrl = res.data.newPhotoUrl;

                    angular.element('#oldPhoto')[0].src = oldPhotoUrl !== null ? oldPhotoUrl : notDataFile;
                    angular.element('#newPhotoUrl')[0].src = newPhotoUrl !== null ? newPhotoUrl : notDataFile;
                    angular.element('#description')[0].innerHTML = res.data.description;

                    clear();
                    angular.element('#images').imageReveal({
                        barWidth: 15,
                        touchBarWidth: 40,
                        paddingLeft: 0,
                        paddingRight: 0,
                        startPosition: 0.25,
                        showCaption: true,
                        captionChange: 0.5,
                        captionFade: 1000,
                        linkCaption: true,
                        width: 500,
                        height: 500
                    });
                });
        }

        function clear() {
            if (angular.element('.imageReveal-drag')[0]) {
                angular.element('.imageReveal-drag')[0].remove();
            }
            if (angular.element('.imageReveal-overlay')[0]) {
                angular.element('.imageReveal-overlay')[0].remove();
            }
            if (angular.element('.imageReveal-background ')[0]) {
                angular.element('.imageReveal-background ')[0].remove();
            }
            if (angular.element('.imageReveal-caption ')[0]) {
                angular.element('.imageReveal-caption ')[0].remove();
            }
        }

        $ctrl.next = function () {
            getPhoto($ctrl.item.nextId)
        };

        $ctrl.next();

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });