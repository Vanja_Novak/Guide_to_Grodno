;(function () {
    "use strict";

    angular.module('app.menu', ['ngRoute', 'ngMaterial'])
        .config(['$routeProvider', config])
        .controller('MenuCtrl', MenuCtrl);

    function config($routeProvider) {
        $routeProvider.when('/test', {
            templateUrl: 'app/menu/menu.html',
            controller: 'MenuCtrl',
            controllerAs: 'vm'
        })
    }

    MenuCtrl.$inject = ['$scope', 'MapService'];
    function MenuCtrl($scope, MapService) {

        $scope.placement = {
            options: [
                'Все',
                'top-left',
                'top-right',
                'bottom',
                'bottom-left',
                'bottom-right',
                'left',
                'left-top',
                'left-bottom',
                'right',
                'right-top',
                'right-bottom'
            ],
            selected: 'Все'
        };

    }
})();