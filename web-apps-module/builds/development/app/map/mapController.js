;(function () {
    "use strict";

    angular.module('app.map', ['ngRoute', 'ngMaterial', 'MapService', 'ngMap', 'ngSanitize', 'ui.bootstrap', 'ui.bootstrap.modal'])
        .config(['$routeProvider', config])
        .controller('MapCtrl', MapCtrl);

    function config($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'app/map/map.html',
            controller: 'MapCtrl',
            controllerAs: 'vm'
        })
    }

    MapCtrl.$inject = ['$mdSidenav', 'NgMap', 'MapService', '$uibModal'];
    function MapCtrl($mdSidenav, NgMap, MapService, $uibModal) {
        var vm = this;

        vm.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDLja5DFT2H1O8y27UEzNs-g8cHKhFe09Y&callback";
        //TODO -- после того, как сделаю настройку карты данные параметры нужно будет брать из куков.
        vm.center = [53.672030, 23.82649];
        vm.zoom = 13;
        vm.groups = {
            selected: null,
            data: []
        };

        vm.rating = {
            selected: null,
            data: [1, 2, 3, 4, 5]
        };
        // Содержит в себе параметры настройки карты и меню.
        vm.data = {
            dataType: "Метки",
            params: null
        };

        NgMap.getMap()
            .then(function (map) {
                vm.map = map;
            });

        vm.showMarker = function (event, item) {
            vm.selectedItem = item;
            vm.map.showInfoWindow('showMarker', this);
        };


        vm.open = function (size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/modal/place/card.html',
                controller: 'PlaceCtrl',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    item: function () {
                        return vm.selectedItem;
                    }
                }
            });
        };

        vm.openNewPlace = function (size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/modal/place/newPlace.html',
                controller: 'PlaceCtrl',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    item: function () {
                        return null;
                    }
                }
            });
        };

        // Работа с панелью навигации >>>
        vm.toggleLeft = buildToggler('leftMenu');
        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            }
        }

        vm.btnFilter = function () {
            init();
        };

        function init() {
            vm.markers = [];
            vm.route = {
                types: null,
                routes: null,
                selectedRoute: null,
                waypoints: [],
                destination: null,
                selectedType: null
            };

            if (vm.data.dataType === 'Метки') {
                if (vm.map !== undefined && vm.map.directionsRenderers !== undefined && vm.map.directionsRenderers[0] !== undefined) {
                    vm.map.directionsRenderers[0].setMap(null);
                }

                var params = {
                    placeGroupCode: vm.groups.selected !== null ? vm.groups.selected.code : null,
                    ratingPlace: vm.rating.selected !== null ? vm.rating.selected : null
                };

                MapService.getPlaceByParams(params)
                    .then(function (response) {
                        vm.markers = response.data;
                    });
            } else if (vm.data.dataType === 'Маршруты') {
                vm.markers = null;


                // Подгружаем список 'Тип маршрута'
                MapService.getAllRouteTypes()
                    .then(function (res) {
                        vm.route.types = res.data;
                    });

                //Подгружаем краткий список маршрутов
                MapService.getAllRoute()
                    .then(function (res) {
                        vm.route.routes = res.data;
                    })
            }

            MapService.getAllPlaceGroups()
                .then(function (response) {
                    vm.groups.data = response.data;
                });
        }

        vm.createRoute = function () {
            if (vm.route.selectedRoute === null || vm.route.selectedRoute.selectedType === null) return;
            vm.route.waypoints = [];
            if (vm.map !== undefined && vm.map.directionsRenderers !== undefined && vm.map.directionsRenderers[0] !== undefined) {
                vm.map.directionsRenderers[0].setMap(vm.map);
            }

            MapService.getRouteId(vm.route.selectedRoute.id)
                .then(function (res) {
                    vm.markers = res.data.places;
                    for (var i = 0; i < vm.markers.length; i++) {
                        vm.route.waypoints.push({
                            location: {
                                lng: vm.markers[i].coordinate[1],
                                lat: vm.markers[i].coordinate[0]
                            },
                            stopover: false
                        })
                    }

                    vm.route.destination = vm.route.waypoints[vm.route.waypoints.length - 1];
                })
        };

        init();

        vm.historyPage = function (size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/modal/history/history.html',
                controller: 'HistoryCtrl',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem
            });
        };

        vm.openGame = function (size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/modal/game/game.html',
                controller: 'GameCtrl',
                controllerAs: '$ctrl',
                appendTo: parentElem
            });
        };


    }
})();