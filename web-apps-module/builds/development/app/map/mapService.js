var MapService = angular.module('MapService', []);

MapService.factory('MapService', ['$http', 'CONSTANT', function ($http, CONSTANT) {

    var URL_PREFIX = CONSTANT.serverURL;
    var URL_PLACE_PREFIX = URL_PREFIX + "/place";
    var URL_ROUTE_PREFIX = URL_PREFIX + "/route";
    var URL_GAME_PREFIX = URL_PREFIX + "/game";

    function getDefaultParams() {
        return {
            '_': new Date().getTime()
        };
    }

    function savePlace(place) {
        var data = "?title=" + place.title + "&name=" + place.name + "&groups=" + place.groups.code + "&description=" + place.description +
            "&latitude=" + place.latitude + "&longitude=" + place.longitude;
        return $http.get(URL_PLACE_PREFIX + '/createPlace' + data, {params: data});
    }

    function getPlaceByParams(params) {
        return $http.get(URL_PLACE_PREFIX + '/', {params: params});
    }

    function getPlaceById(id) {
        return $http.get(URL_PLACE_PREFIX + '/' + id, {'params': getDefaultParams()});
    }

    function getAllPlaceGroups() {
        return $http.get(URL_PLACE_PREFIX + '/groups/', {params: getDefaultParams()});
    }

    function createCommentPlace(data) {
        return $http.get(URL_PLACE_PREFIX + '/comments/' + data.entityId + '/' + data.message + "/" + data.rating, {'params': getDefaultParams()});
    }

    function getPlaceComments(id) {
        return $http.get(URL_PLACE_PREFIX + '/comments/' + id, {'params': getDefaultParams()});
    }

    function getHistoryPlace(params) {
        return $http.get(URL_PLACE_PREFIX + '/history', {params: params});
    }

    function entrancePointToCircle(params) {
        return $http.get(URL_GAME_PREFIX + '/entrancePointToCircle', {params: params});
    }

    function nextGame() {
        return $http.get(URL_GAME_PREFIX + '/next', {params: getDefaultParams()});
    }

    function getAllRoute() {
        return $http.get(URL_ROUTE_PREFIX + '/', {params: getDefaultParams()});
    }

    function getAllRouteTypes() {
        return $http.get(URL_ROUTE_PREFIX + '/routeType', {params: getDefaultParams()});
    }

    function getRouteId(id) {
        return $http.get(URL_ROUTE_PREFIX + '/' + id, {params: getDefaultParams()});
    }

    return {
        'savePlace': savePlace,
        'getPlaceByParams': getPlaceByParams,
        'getPlaceById': getPlaceById,
        'getAllPlaceGroups': getAllPlaceGroups,
        'createCommentPlace': createCommentPlace,
        'getPlaceComments': getPlaceComments,
        'getAllRoute': getAllRoute,
        'getHistoryPlace': getHistoryPlace,
        'entrancePointToCircle': entrancePointToCircle,
        'nextGame': nextGame,
        'getAllRouteTypes': getAllRouteTypes,
        'getRouteId': getRouteId
    };
}
]);