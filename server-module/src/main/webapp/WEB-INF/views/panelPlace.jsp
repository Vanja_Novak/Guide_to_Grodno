<%@ page contentType="text/html;charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Маркеры</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/offcanvas.css' />" rel="stylesheet"/>
    <script src="<c:url value="/static/js/jquery-latest.min.js" />"></script>
    <script src="<c:url value="/static/js/jquery.tablesorter.min.js" />"></script>
    <script type="text/javascript">
        $(document).ready(function () {
                $("#placeTable").tablesorter({
                    headers: {
                        4: {sorter: false}
                    }
                });
            }
        );
    </script>
</head>
<body>
<%@include file="panelHeader.jsp" %>

<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading">Список всех меток в системе</div>

                <table class="table tablesorter" id="placeTable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Группа</th>
                        <th>check</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${places}" var="place">
                        <tr>
                            <td>${place.id}</td>
                            <td>${place.title}</td>
                            <td>${place.groupDesk}</td>
                            <td>
                                <c:choose>
                                    <c:when test="${place.moderation == true}">
                                        <span style="color: green">Да</span>
                                    </c:when>
                                    <c:otherwise>
                                        <span style="color: red">Нет</span>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                <a href="<c:url value='/admin/edit-place-${place.id}' />"
                                   class="glyphicon glyphicon-pencil"></a>
                                <a href="<c:url value='/admin/remove-place-${place.id}' />"
                                   class="glyphicon glyphicon-trash"></a>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="list-group">
                <a href="<c:url value='/admin/panel'/>" class="list-group-item active">Список меток</a>
                <a href="<c:url value='/admin/create-pace'/>" class="list-group-item">Создание метки</a>
                <a href="<c:url value='/admin/panel-admin-places'/>" class="list-group-item">Администрирование меток</a>
                <a href="<c:url value='/admin/panel-routing'/>" class="list-group-item">Маршруты</a>
                <a href="<c:url value='/admin/create-route'/>" class="list-group-item">Создание маршрута</a>
                <a href="#" class="list-group-item">Администрирование пользователей</a>
            </div>
        </div>
    </div>
    <hr>
</div>
</body>
</html>