<%@ page contentType="text/html;charset=utf-8" %>

<nav class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<c:url value='/admin/panel'/>" class="navbar-brand">Путеводитель Гродно</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li style="color: white">
                Вы вошли как: <strong>${loggedinuser}</strong>
            <li>
                <a href="<c:url value="/admin/logout" />">Выйти</a>
            </li>
        </ul>
    </div>
</nav>