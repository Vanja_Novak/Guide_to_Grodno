<%@ page contentType="text/html;charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="f" uri="http://edu.grsu.guide.com/functions" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Карточка маршрута</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/offcanvas.css' />" rel="stylesheet"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/jquery-latest.min.js"></script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/static/js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/js/app.js"></script>
</head>

<body>
<%@include file="panelHeader.jsp" %>

<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <form:form method="POST" modelAttribute="route" class="form-horizontal">
                <form:input type="hidden" path="id" id="id"/>
                <h3>Карточка маршрута
                    <c:if test="${route.id !=null}">
                        [ID  ${route.id}]
                    </c:if>
                </h3>
                <c:if test="${route.updateDate !=null}">
                    <h5>
                        Последний раз редактировалось:
                        <fmt:formatDate type="both"
                                        dateStyle="short" timeStyle="short"
                                        value="${route.updateDate}"/>
                    </h5>
                </c:if>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Заголовок</label>
                    <div class="col-lg-8">
                        <form:input type="text" path="title" id="title" class="form-control input-sm"
                                    required="required"/>
                        <div class="has-error">
                            <form:errors path="title" class="help-inline"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Описание</label>
                    <div class="col-lg-8">
                        <form:textarea rows="5" cols="30" path="description" id="description"
                                       class="form-control input-sm" required="required"/>
                        <div class="has-error">
                            <form:errors path="description" class="help-inline"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Метки</label>
                    <div class="col-lg-8">
                        <form:select path="places" multiple="true" class="form-control input-sm">
                            <c:forEach items="${places}" var="place">
                                <option value="${place.id}" ${f:contains(route.places, place.id) ? 'selected' : ''}>${place.title}</option>
                            </c:forEach>
                        </form:select>
                        <div class="has-error">
                            <form:errors path="places" class="help-inline"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-8">
                        <form:checkbox path="moderation" id="moderation" class="checkbox-inline"/> Провалидированная
                        запись
                        <div class="has-error">
                            <form:errors path="moderation" class="help-inline"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-actions floatRight">
                        <c:choose>
                            <c:when test="${edit}">
                                <input type="submit" value="Обновить" class="btn btn-primary btn-sm"/> или <a
                                    href="<c:url value='/admin/panel' />">Выйти</a>
                            </c:when>
                            <c:otherwise>
                                <input type="submit" value="Сохранить" class="btn btn-primary btn-sm"/> или <a
                                    href="<c:url value='/admin/panel' />">Выйти</a>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>


            </form:form>
        </div>

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="list-group">
                <a href="<c:url value='/admin/panel'/>" class="list-group-item">Список меток</a>
                <a href="<c:url value='/admin/create-pace'/>" class="list-group-item">Создание метки</a>
                <a href="<c:url value='/admin/panel-admin-places'/>" class="list-group-item">Администрирование меток</a>
                <a href="<c:url value='/admin/panel-routing'/>" class="list-group-item">Маршруты</a>
                <a href="<c:url value='/admin/create-route'/>" class="list-group-item active">Создание маршрута</a>
                <a href="#" class="list-group-item">Администрирование пользователей</a>
            </div>
        </div>
    </div>
    <hr>
</div>
</body>
</html>