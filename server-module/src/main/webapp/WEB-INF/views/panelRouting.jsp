<%@ page contentType="text/html;charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Маршруты</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/offcanvas.css' />" rel="stylesheet"/>
    <script src="<c:url value="/static/js/jquery-latest.min.js" />"></script>
    <script src="<c:url value="/static/js/jquery.tablesorter.min.js" />"></script>
    <script type="text/javascript">
        $(document).ready(function () {
                $("#routeTable").tablesorter({
                    headers: {
                        4: {sorter: false}
                    }
                });
            }
        );
    </script>
</head>
<body>
<%@include file="panelHeader.jsp" %>

<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading">Список всех маршрутов в системе</div>
                <table class="table" id="routeTable">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Название</th>
                        <th>Количество меток</th>
                        <th>Проверен ?</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${routes}" var="route">
                    <tr>
                        <td>${route.id}</td>
                        <td>${route.title}</td>
                        <td>${route.places.size()}</td>
                        <td>
                            <c:choose>
                                <c:when test="${route.moderation == true}">
                                    <span style="color: green">Да</span>
                                </c:when>
                                <c:otherwise>
                                    <span style="color: red">Нет</span>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td><a href="<c:url value='/admin/edit-route-${route.id}' />"
                               class="btn btn-success custom-width">Edit</a></td>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="list-group">
                <a href="<c:url value='/admin/panel'/>" class="list-group-item">Список меток</a>
                <a href="<c:url value='/admin/create-pace'/>" class="list-group-item">Создание метки</a>
                <a href="<c:url value='/admin/panel-admin-places'/>" class="list-group-item">Администрирование меток</a>
                <a href="<c:url value='/admin/panel-routing'/>" class="list-group-item active">Маршруты</a>
                <a href="<c:url value='/admin/create-route'/>" class="list-group-item">Создание маршрута</a>
                <a href="#" class="list-group-item">Администрирование пользователей</a>
            </div>
        </div>
    </div>
</div>
<hr>
</body>
</html>