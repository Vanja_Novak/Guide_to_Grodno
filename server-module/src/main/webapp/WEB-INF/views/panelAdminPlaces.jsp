<%@ page contentType="text/html;charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Панель Администрирования Меток</title>
    <link href="<c:url value='/static/css/bootstrap.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"/>
    <link href="<c:url value='/static/css/offcanvas.css' />" rel="stylesheet"/>
    <script src="<c:url value="/static/js/jquery-3.2.1.min.js" />"></script>
    <script src="<c:url value="/static/js/bootstrap.min.js" />"></script>
    <script type="text/javascript">
        $(function () {
            $('img').on('click', function () {
                $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
            });
        });
    </script>
</head>
<body>
<%@include file="panelHeader.jsp" %>

<div class="container">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-xs-12 col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading">Администрирование меток</div>

                <div class="row" style="margin: 10px">
                    <div class="col-lg-6">
                        <%--//TODO пока статический текст. Потом нужно выводить реалиное время синхронизации данных. --%>
                        <span> Последний раз база Фото обновлялась 22.04.2017 14:52</span>
                        <c:if test="${success}">
                            <p style="color: #5cb85c">Данные успешно обновлены!</p>
                        </c:if>
                    </div>

                    <div class="col-lg-6">
                        <a href="<c:url value='/admin/update-image-base' />"
                           class="btn btn-success">Синхронизовать базу фотографий</a>
                    </div>
                </div>

                <div class="row" style="padding-left: 10px;">
                    <c:forEach items="${images}" var="image">
                        <div class="col-xs-6 col-md-3" style="    width: 30%;">
                            <a href="#" class="thumbnail" data-toggle="modal" data-target="#myModal">
                                <img src="${image.fileUrl}" style="max-height: 120px">
                            </a>
                        </div>
                    </c:forEach>
                </div>
            </div>
        </div>

        <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="list-group">
                <a href="<c:url value='/admin/panel'/>" class="list-group-item">Список меток</a>
                <a href="<c:url value='/admin/create-pace'/>" class="list-group-item">Создание метки</a>
                <a href="<c:url value='/admin/panel-admin-places'/>" class="list-group-item active">Администрирование
                    меток</a>
                <a href="<c:url value='/admin/panel-routing'/>" class="list-group-item">Маршруты</a>
                <a href="<c:url value='/admin/create-route'/>" class="list-group-item">Создание маршрута</a>
                <a href="#" class="list-group-item">Администрирование пользователей</a>
            </div>
        </div>
    </div>
</div>
<hr>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <img src="" class="enlargeImageModalSource" style="width: 100%;">
        </div>
    </div>
</div>
</body>
</html>