<%@ page contentType="text/html;charset=utf-8" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Доступ запрещен</title>
</head>
<body>
<div class="generic-container">
    <div class="authbar">
        <span>Уважаемый <strong>${loggedinuser}</strong>, Вам запрещен доступ к данной странице</span> <span
            class="floatRight"><a href="<c:url value="/logout" />">Выйти</a></span>
    </div>
</div>
</body>
</html>