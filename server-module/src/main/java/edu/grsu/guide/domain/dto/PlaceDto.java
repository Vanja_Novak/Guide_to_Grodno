package edu.grsu.guide.domain.dto;

import java.sql.Timestamp;
import java.util.List;

/**
 * User: Vanja Novak
 * Date : 11.02.2017
 * Time : 11:40
 */
public class PlaceDto {

    private Integer id;

    private String type;

    private Boolean moderation;

    private String title;

    private String name;

    private String description;

    private PlaceGroupShortDto placeGroup;

    private CoordinateDto coordinate;

    private List<CommentDto> comments;

    private List<ImageDto> images;

    private Timestamp updateDate;

    private Double totalRating;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlaceGroupShortDto getPlaceGroup() {
        return placeGroup;
    }

    public void setPlaceGroup(PlaceGroupShortDto placeGroup) {
        this.placeGroup = placeGroup;
    }

    public List<CommentDto> getComments() {
        return comments;
    }

    public void setComments(List<CommentDto> comments) {
        this.comments = comments;
    }

    public List<ImageDto> getImages() {
        return images;
    }

    public void setImages(List<ImageDto> images) {
        this.images = images;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getModeration() {
        return moderation;
    }

    public void setModeration(Boolean moderation) {
        this.moderation = moderation;
    }

    public CoordinateDto getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(CoordinateDto coordinate) {
        this.coordinate = coordinate;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public Double getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Double totalRating) {
        this.totalRating = totalRating;
    }
}
