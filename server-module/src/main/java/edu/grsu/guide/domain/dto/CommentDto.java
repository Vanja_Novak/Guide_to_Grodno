package edu.grsu.guide.domain.dto;

import java.sql.Timestamp;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 15:06
 */
public class CommentDto {

    private Long id;
    private String text;
    private Timestamp createDate;
    private Integer rating;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}