package edu.grsu.guide.domain.dto;

/**
 * @author Vanja Novak
 * @version 1.0 28.04.2017 13:15
 */
public class ResponseGameDto {

    /**
     * IP клиента
     */
    private String ipClient;

    /**
     * Метров до точки
     */
    private Integer meters;

    /**
     * Попал ?
     */
    private boolean hasGot = false;

    /**
     * Если попал, то кол-во очков
     */
    private Integer points;

    private String description;

    public String getIpClient() {
        return ipClient;
    }

    public void setIpClient(String ipClient) {
        this.ipClient = ipClient;
    }

    public Integer getMeters() {
        return meters;
    }

    public void setMeters(Integer meters) {
        this.meters = meters;
    }

    public boolean isHasGot() {
        return hasGot;
    }

    public void setHasGot(boolean hasGot) {
        this.hasGot = hasGot;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}