package edu.grsu.guide.domain;

/**
 * Справочник тип точки
 * <p>
 * User: Vanja Novak
 * Date : 11.02.2017
 * Time : 11:34
 */
public enum CoordinatesType {
    /**
     * Точка
     */
    Point,

    /**
     * Маршрут
     */
    viaPoint
}