package edu.grsu.guide.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 17:07
 */
@Entity
@Table(name = "Coordinates")
public class Coordinates extends BaseEntity {

    @NotNull
    @Column(name = "LATITUDE", nullable = false)
    private Double latitude;

    @NotNull
    @Column(name = "LONGITUDE", nullable = false)
    private Double longitude;

    @Enumerated(EnumType.STRING)
    @Column(name = "TYPE", nullable = false)
    private CoordinatesType coordinatesType;

    @Transient
    private List<Double> coordinates;

    public Coordinates() {
    }

    public Coordinates(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.coordinatesType = CoordinatesType.Point;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public CoordinatesType getCoordinatesType() {
        return coordinatesType;
    }

    public void setCoordinatesType(CoordinatesType coordinatesType) {
        this.coordinatesType = coordinatesType;
    }

    public List<Double> getCoordinates() {
        return new ArrayList<Double>() {{
            add(longitude);
            add(latitude);
        }};
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
}
