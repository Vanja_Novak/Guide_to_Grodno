package edu.grsu.guide.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 16:53
 */
@Entity
@Table(name = "PlaceGroups")
public class PlaceGroups extends BaseEntity {

    /**
     * Заголовок
     */
    @Column(name = "TITLE", nullable = false)
    private String title;

    /**
     * Описание
     */
    @Column(name = "DESCRIPTION", nullable = false, length = 1024)
    private String description;

    /**
     * Код группы
     */
    @Column(name = "CODE", nullable = false)
    private String code;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
