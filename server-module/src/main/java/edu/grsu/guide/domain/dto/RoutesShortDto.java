package edu.grsu.guide.domain.dto;

import java.sql.Timestamp;

/**
 * DTO - с кратким содержанием всех маршрутов в системе.
 *
 * @author Vanja Novak
 * @version 1.0 29.04.2017 22:56
 */
public class RoutesShortDto {

    private Integer id;
    private String title;
    private String description;
    private Integer countPlaces;
    private Timestamp updateDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCountPlaces(int size) {
        return countPlaces;
    }

    public void setCountPlaces(Integer countPlaces) {
        this.countPlaces = countPlaces;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }
}
