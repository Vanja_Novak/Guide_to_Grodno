package edu.grsu.guide.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:02
 */
@Entity
@Table(name = "USER_PROFILE")
public class UserProfile extends BaseEntity implements Serializable {

    @Column(name = "TYPE", length = 15, unique = true, nullable = false)
    private String type = UserProfileType.ADMIN.getUserProfileType();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof UserProfile))
            return false;
        UserProfile other = (UserProfile) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
}