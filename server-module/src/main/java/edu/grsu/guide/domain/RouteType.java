package edu.grsu.guide.domain;

/**
 * @author Vanja Novak
 * @version 1.0 29.04.2017 22:47
 */
public enum RouteType {

    DRIVING("Автомобиль"),
    BICYCLING("Велосипед"),
    //    TRANSIT("Общественный транспорт "),
    WALKING("Пешеход");

    private String title;

    RouteType(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}