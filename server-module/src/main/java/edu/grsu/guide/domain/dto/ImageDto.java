package edu.grsu.guide.domain.dto;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 22:12
 */
public class ImageDto {

    private Integer id;

    private String fileUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }
}