package edu.grsu.guide.domain.dto;

/**
 * User: Vanja Novak
 * Date : 23.02.2017
 * Time : 13:07
 */
public class PlaceGroupShortDto {

    private String code;

    private String title;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
