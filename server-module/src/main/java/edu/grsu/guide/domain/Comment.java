package edu.grsu.guide.domain;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * Таблица для хранения комментариев.
 *
 * @author : Vanja Novak
 *         Date : 05.03.2017
 *         Time : 14:57
 */
@Entity
@Table(name = "Comment")
public class Comment extends BaseEntity {

    @CreationTimestamp
    @Column(name = "CREATE_DATE")
    private Timestamp createDate;

    @Column(name = "RATING")
    private Integer rating;

    @Column(name = "TEXT", length = 1024, nullable = false)
    private String text;

    public Comment() {
    }

    public Comment(String text, Integer rating) {
        this.text = text;
        this.rating = rating;
        this.createDate = Timestamp.from(Instant.now());
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}