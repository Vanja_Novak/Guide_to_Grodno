package edu.grsu.guide.domain.dto;

import edu.grsu.guide.domain.ImageType;

/**
 * @author Vanja Novak
 * @version 1.0 26.03.2017 12:26
 */
public class DataStoreImageDto {

    private String imageUrl;

    private ImageType imageType;

    public DataStoreImageDto(String imageUrl, ImageType imageType) {
        this.imageUrl = imageUrl;
        this.imageType = imageType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }
}
