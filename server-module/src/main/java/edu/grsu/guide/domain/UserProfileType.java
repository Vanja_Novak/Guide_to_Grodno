package edu.grsu.guide.domain;

import java.io.Serializable;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:01
 */

public enum UserProfileType implements Serializable {

    DBA("DBA"),
    ADMIN("ADMIN");

    String userProfileType;

    private UserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType() {
        return userProfileType;
    }
}