package edu.grsu.guide.domain;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 16:37
 */
@Entity
@Table(name = "Place")
public class Place extends BaseEntity {

    /**
     * Название
     */
    @NotNull
    @Column(name = "NAME")
    private String name;

    /**
     * Заголовок
     */
    @NotNull
    @Column(name = "TITLE")
    private String title;

    /**
     * Описание
     */
    @Column(name = "DESCRIPTION", length = 1024)
    private String description;

    /**
     * Прошла ли метка модерацию
     */
    @Column(name = "MODERATION")
    private Boolean moderation;

    /**
     * Дата и время полследнего обновления сущности.
     */
    @UpdateTimestamp
    @Column(name = "UPDATE_DATE")
    private Timestamp updateDate;

    /**
     * Группа
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GROUP_ID")
    private PlaceGroups group;

    /**
     * Координаты
     */
    // @NotNull
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "COORDINATE_ID")
    private Coordinates coordinate;

    /**
     * Фото.
     */
    @Transient
    private List<Image> images = new ArrayList<>();

    /**
     * Комментраии
     */
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "PlaceComments", catalog = "Guide", joinColumns = {
            @JoinColumn(name = "PLACE_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "COMMENT_ID",
                    nullable = false, updatable = false)})
    private Set<Comment> comments;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PlaceGroups getGroup() {
        return group;
    }

    public void setGroup(PlaceGroups group) {
        this.group = group;
    }

    public Coordinates getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinates coordinate) {
        this.coordinate = coordinate;
    }

    public Boolean getModeration() {
        return moderation;
    }

    public void setModeration(Boolean moderation) {
        this.moderation = moderation;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
