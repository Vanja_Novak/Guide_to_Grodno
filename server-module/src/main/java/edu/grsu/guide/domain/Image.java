package edu.grsu.guide.domain;

import javax.persistence.*;

/**
 * Фото
 * <p>
 * User: Vanja Novak
 * Date : 12.02.2017
 * Time : 16:40
 */
@Entity
@Table(name = "Image")
public class Image extends BaseEntity {

    @Column(name = "FILE_URL")
    private String fileUrl;

    @Column(name = "IMAGE_TYPE")
    @Enumerated(EnumType.STRING)
    private ImageType imageType;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PLACE_ID")
    private Place place;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public ImageType getImageType() {
        return imageType;
    }

    public void setImageType(ImageType imageType) {
        this.imageType = imageType;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }
}