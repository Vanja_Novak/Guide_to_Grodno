package edu.grsu.guide.domain.dto;

import java.sql.Timestamp;
import java.util.List;

/**
 * User: Vanja Novak
 * Date : 11.02.2017
 * Time : 13:11
 */
public class RouteDto extends RoutesShortDto {

    private List<PlaceShortDto> places;

    private Boolean moderation;

    public List<PlaceShortDto> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceShortDto> places) {
        this.places = places;
    }

    public Boolean getModeration() {
        return moderation;
    }

    public void setModeration(Boolean moderation) {
        this.moderation = moderation;
    }
}