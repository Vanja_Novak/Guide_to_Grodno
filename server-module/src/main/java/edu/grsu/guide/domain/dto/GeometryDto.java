package edu.grsu.guide.domain.dto;

import edu.grsu.guide.domain.CoordinatesType;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 11.02.2017
 * Time : 13:36
 */
public class GeometryDto {

    private CoordinatesType type;

    private List<Double> coordinates;

    public CoordinatesType getType() {
        return type;
    }

    public void setType(CoordinatesType type) {
        this.type = type;
    }

    public List<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<Double> coordinates) {
        this.coordinates = coordinates;
    }
}