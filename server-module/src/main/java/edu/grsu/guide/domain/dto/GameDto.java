package edu.grsu.guide.domain.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 28.04.2017 5:10
 */
public class GameDto {

    private Integer placeId;
    private String photoUrl;
    private List<String> photos = new ArrayList<>();

    public Integer getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Integer placeId) {
        this.placeId = placeId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<String> getPhotos() {
        return photos;
    }

    public void setPhotos(List<String> photos) {
        this.photos = photos;
    }
}