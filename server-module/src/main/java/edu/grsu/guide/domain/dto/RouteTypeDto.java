package edu.grsu.guide.domain.dto;

/**
 * @author Vanja Novak
 * @version 1.0 29.04.2017 23:15
 */
public class RouteTypeDto {

    private String value;
    private String title;

    public RouteTypeDto() {
    }

    public RouteTypeDto(String value, String title) {
        this.value = value;
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
