package edu.grsu.guide.domain;

/**
 * @author Vanja Novak
 * @version 1.0 26.03.2017 12:23
 */
public enum ImageType {
    /**
     * Фотография сделана недавно
     */
    NEW,
    /**
     * Старая фотография
     */
    OLD;
}
