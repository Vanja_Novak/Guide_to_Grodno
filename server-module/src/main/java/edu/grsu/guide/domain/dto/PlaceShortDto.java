package edu.grsu.guide.domain.dto;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 12.02.2017
 * Time : 15:29
 */
public class PlaceShortDto {

    private Integer id;

    private String type;

    private String groupDesk;

    private List<Double> coordinate;

    private String title;

    private String description;

    private Boolean moderation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(List<Double> coordinate) {
        this.coordinate = coordinate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getModeration() {
        return moderation;
    }

    public void setModeration(Boolean moderation) {
        this.moderation = moderation;
    }

    public String getGroupDesk() {
        return groupDesk;
    }

    public void setGroupDesk(String groupDesk) {
        this.groupDesk = groupDesk;
    }
}
