package edu.grsu.guide.domain.dto;

/**
 * @author Vanja Novak
 * @version 1.0 11.03.2017 3:19
 */
public class CoordinateDto {

    private Integer id;
    private Double latitude;
    private Double longitude;

    public CoordinateDto() {
    }

    public CoordinateDto(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
