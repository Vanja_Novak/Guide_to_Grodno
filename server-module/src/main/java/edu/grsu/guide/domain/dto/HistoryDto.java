package edu.grsu.guide.domain.dto;

/**
 * @author Vanja Novak
 * @version 1.0 27.04.2017 23:09
 */
public class HistoryDto {

    private String newPhotoUrl;
    private String oldPhotoUrl;
    private String description;
    private Integer nextId;

    public String getNewPhotoUrl() {
        return newPhotoUrl;
    }

    public void setNewPhotoUrl(String newPhotoUrl) {
        this.newPhotoUrl = newPhotoUrl;
    }

    public String getOldPhotoUrl() {
        return oldPhotoUrl;
    }

    public void setOldPhotoUrl(String oldPhotoUrl) {
        this.oldPhotoUrl = oldPhotoUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNextId() {
        return nextId;
    }

    public void setNextId(Integer nextId) {
        this.nextId = nextId;
    }
}
