package edu.grsu.guide.configuration;

import edu.grsu.guide.support.converter.PlaceConverter;
import edu.grsu.guide.support.converter.PlaceGroupToConverter;
import edu.grsu.guide.support.converter.RoleToUserProfileConverter;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.ArrayList;

/**
 * Конфигурация придожения.
 * <p>
 * Created by Vanja Novak
 * Date : 09.10.2016
 * Time : 0:39
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "edu.grsu.guide")
public class AppConfig extends WebMvcConfigurerAdapter {

    @Autowired
    RoleToUserProfileConverter roleToUserProfileConverter;
    @Autowired
    PlaceGroupToConverter placeGroupToConverter;
    @Autowired
    PlaceConverter placeConverter;

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        registry.viewResolver(viewResolver);
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer matcher) {
        matcher.setUseRegisteredSuffixPatternMatch(true);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("/static/");
    }

    @Bean
    public Mapper mapper() {
        return new DozerBeanMapper(new ArrayList<String>() {{
            add("dozerMapping.xml");
        }});
    }


    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(roleToUserProfileConverter);
        registry.addConverter(placeGroupToConverter);
        registry.addConverter(placeConverter);
    }

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages");
        return messageSource;
    }

}