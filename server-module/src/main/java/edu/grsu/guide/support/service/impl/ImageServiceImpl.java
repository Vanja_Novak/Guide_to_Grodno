package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.dto.ImageDto;
import edu.grsu.guide.support.dao.ImageDao;
import edu.grsu.guide.support.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 21:25
 */
@Service("imageService")
public class ImageServiceImpl extends BasicService<Image, ImageDto> implements ImageService {

    @Autowired
    private ImageDao imageDao;

    @Override
    public void saveImage(Image image) {
        imageDao.saveOrUpdate(image);
    }

    @Override
    public void deleteImages() {
        imageDao.deleteImages();
    }

    @Override
    public List<Image> getAll() {
        return imageDao.getAll();
    }

    @Override
    public ImageDto getById(Integer id) {
        return null;
    }
}