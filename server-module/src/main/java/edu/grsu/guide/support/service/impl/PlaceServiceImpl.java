package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.Coordinates;
import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.dto.*;
import edu.grsu.guide.support.converter.CustomConverter;
import edu.grsu.guide.support.dao.CommentDao;
import edu.grsu.guide.support.dao.CoordinateDao;
import edu.grsu.guide.support.dao.PlaceDao;
import edu.grsu.guide.support.service.ImageService;
import edu.grsu.guide.support.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 18:24
 */
@Service("placeService")
public class PlaceServiceImpl extends BasicService<Place, PlaceDto> implements PlaceService {

    public static final String DTO_ASS_PLACE = "place";

    public static final String DTO_ASS_PLACE_SHORT = "placeShort";

    @Autowired
    private PlaceDao placeDao;
    @Autowired
    private CoordinateDao coordinateDao;
    @Autowired
    private ImageService imageService;
    @Autowired
    private CommentDao commentDao;

    public List<Place> getAll() {
        return convertToList(placeDao.getAll(), Place.class, DTO_ASS_PLACE_SHORT);
    }

    public PlaceDto getById(Integer id) {
        PlaceDto dto = convert(placeDao.getById(id), PlaceDto.class, DTO_ASS_PLACE);
        dto.setTotalRating(commentDao.getTotalRatingPlace(dto.getId()));
        return dto;
    }

    @Override
    public List<Place> getPlaceByParam(String placeGroupCode, Integer ratingPlace) {
        return convertToList(placeDao.getPlaceByParam(placeGroupCode, ratingPlace), Place.class, DTO_ASS_PLACE_SHORT);
    }

    @Override
    public List<Place> getPlacesFullField() {
        return placeDao.getAll();
    }

    @Override
    public Place getPlaceForUpdate(Integer id) {
        return placeDao.getById(id);
    }

    @Override
    public Place getByCoordinate(Double latitude, Double longitude) {
        return placeDao.getByCoordinate(latitude, longitude);
    }

    @Override
    public boolean existPlaceForCoordinate(Double latitude, Double longitude) {
        return placeDao.existPlaceForCoordinate(latitude, longitude);
    }

    @Override
    public void bindImagesForPlace(Place place, Set<DataStoreImageDto> images) {

        images.forEach(item -> {
            Image image = new Image();
            image.setFileUrl(item.getImageUrl());
            image.setImageType(item.getImageType());
            image.setPlace(place);
            imageService.saveImage(image);
        });

        placeDao.saveOrUpdate(place);
    }

    @Override
    @Transactional
    public void updateOrCreatePlace(Place placeModify) {

        Place place;
        if (placeModify.getId() != null) {
            //Update
            place = placeDao.getById(placeModify.getId());
            place.setTitle(CustomConverter.getEncodingText(placeModify.getTitle()));
            place.setName(CustomConverter.getEncodingText(placeModify.getName()));
            place.setDescription(CustomConverter.getEncodingText(placeModify.getDescription()));
            place.setModeration(placeModify.getModeration());
            place.setUpdateDate(Timestamp.from(Instant.now()));
            place.setGroup(placeModify.getGroup());
            place.getCoordinate().setLatitude(placeModify.getCoordinate().getLatitude());
            place.getCoordinate().setLongitude(placeModify.getCoordinate().getLongitude());
        } else {
            place = new Place();
            Coordinates coordinates = new Coordinates(placeModify.getCoordinate().getLatitude(), placeModify.getCoordinate().getLongitude());
            coordinateDao.saveOrUpdate(coordinates);
            place.setCoordinate(coordinates);
            place.setTitle(CustomConverter.getEncodingText(placeModify.getTitle()));
            place.setName(CustomConverter.getEncodingText(placeModify.getName()));
            place.setDescription(CustomConverter.getEncodingText(placeModify.getDescription()));
            place.setModeration(placeModify.getModeration());
            place.setUpdateDate(Timestamp.from(Instant.now()));
            place.setGroup(placeModify.getGroup());
        }

        placeDao.saveOrUpdate(place);
    }

    @Override
    public HistoryDto getHistoryPlace(Integer id) {
        return placeDao.getHistoryPlace(id);
    }

    @Override
    public ResponseGameDto entrancePointToCircle(Integer placeId, Double latitude, Double longitude) {
        ResponseGameDto responseGameDto = placeDao.entrancePointToCircle(placeId, latitude, longitude);

        Integer meters = responseGameDto.getMeters();

        if (meters != null && responseGameDto.isHasGot()) {
            if (meters >= 400) {
                responseGameDto.setPoints(1);
            } else if (meters >= 300) {
                responseGameDto.setPoints(2);
            } else if (meters >= 200) {
                responseGameDto.setPoints(3);
            } else if (meters >= 100) {
                responseGameDto.setPoints(4);
            } else {
                responseGameDto.setPoints(5);
            }
        }

        return responseGameDto;
    }

    @Override
    public GameDto game() {
        return placeDao.game();
    }

    @Override
    public void removePlace(Integer placeId) {
        placeDao.removePlace(placeId);
    }
}
