package edu.grsu.guide.support.service;

/**
 * @author Vanja Novak
 * @version 1.0 25.03.2017 14:41
 */
public interface DropBoxService {

    /**
     * Синхронизирование данных в облаке и в БД.
     */
    void synchronization();

}