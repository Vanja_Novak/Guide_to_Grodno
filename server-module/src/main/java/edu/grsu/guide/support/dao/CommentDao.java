package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.Comment;

import java.util.Set;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 19:11
 */
public interface CommentDao extends AbstractDao<Comment> {

    Set<Comment> getCommentsForPlace(Integer placeId);

    void createCommentForPlace(Integer placeId, String message, Integer rating);

    Double getTotalRatingPlace(Integer placeId);
}