package edu.grsu.guide.support.converter;

import java.io.UnsupportedEncodingException;

/**
 * @author Vanja Novak
 * @version 1.0 09.03.2017 16:18
 */
public class CustomConverter {

    public static String getEncodingText(String text) {
        byte ptext[];
        try {
            ptext = text.getBytes("ISO-8859-1");
            return new String(ptext, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}