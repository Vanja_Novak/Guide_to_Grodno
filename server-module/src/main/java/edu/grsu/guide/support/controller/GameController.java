package edu.grsu.guide.support.controller;

import edu.grsu.guide.support.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Vanja Novak
 * @version 1.0 28.04.2017 5:11
 */
@Controller
@RequestMapping(value = "/api/game")
public class GameController extends BasicController {

    @Autowired
    private PlaceService placeService;

    @RequestMapping(value = "/entrancePointToCircle", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> entrancePointToCircle(
            @RequestParam(value = "placeId") Integer placeId,
            @RequestParam(value = "latitude") Double latitude,
            @RequestParam(value = "longitude") Double longitude) {
        return buildSuccess(placeService.entrancePointToCircle(placeId, latitude, longitude));
    }

    @RequestMapping(value = "/next", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> game() {
        return buildSuccess(placeService.game());
    }
}