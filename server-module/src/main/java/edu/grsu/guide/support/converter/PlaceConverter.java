package edu.grsu.guide.support.converter;

import edu.grsu.guide.domain.Place;
import edu.grsu.guide.support.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 11:55
 */
@Component
public class PlaceConverter implements Converter<String, Place> {

    @Autowired
    private PlaceService placeService;

    @Override
    public Place convert(String s) {
        return placeService.getPlaceForUpdate(Integer.parseInt(s));
    }
}