package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.dto.GameDto;
import edu.grsu.guide.domain.dto.HistoryDto;
import edu.grsu.guide.domain.dto.PlaceDto;
import edu.grsu.guide.domain.dto.ResponseGameDto;

import java.util.List;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 17:22
 */
public interface PlaceDao extends AbstractDao<Place> {

    List<PlaceDto> getPlaceByParam(String placeGroupCode, Integer ratingPlace);

    Place getByCoordinate(Double latitude, Double longitude);

    boolean existPlaceForCoordinate(Double latitude, Double longitude);

    HistoryDto getHistoryPlace(Integer id);

    ResponseGameDto entrancePointToCircle(Integer placeId, Double latitude, Double longitude);

    GameDto game();

    void removePlace(Integer placeId);
}
