package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.dto.CommentDto;

import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 19:09
 */
public interface CommentService {

    List<CommentDto> getCommentsForPlace(Integer placeId);

    void createCommentForPlace(Integer placeId, String message, Integer rating);

}