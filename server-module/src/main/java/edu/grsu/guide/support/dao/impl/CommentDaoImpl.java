package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.Comment;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.support.dao.CommentDao;
import edu.grsu.guide.support.dao.PlaceDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 19:11
 */
@Repository("commentDao")
public class CommentDaoImpl extends AbstractDaoImpl<Integer, Comment> implements CommentDao {

    @Autowired
    private PlaceDao placeDao;

    @Override
    @Transactional
    public Set<Comment> getCommentsForPlace(Integer placeId) {
        Criteria criteria = getSession().createCriteria(Place.class);
        criteria.add(Restrictions.eq("id", placeId));
        Place place = (Place) criteria.uniqueResult();
        return place.getComments();
    }

    @Override
    @Transactional
    public void createCommentForPlace(Integer placeId, String message, Integer rating) {
        Criteria criteria = getSession().createCriteria(Place.class);
        criteria.add(Restrictions.eq("id", placeId));
        Place place = (Place) criteria.uniqueResult();

        place.getComments().add(new Comment(message, rating));
        placeDao.saveOrUpdate(place);
    }

    @Override
    @Transactional
    public Double getTotalRatingPlace(Integer placeId) {
        Criteria criteria = getSession().createCriteria(Place.class);
        criteria.add(Restrictions.eq("id", placeId));

        Place place = (Place) criteria.list().stream().findFirst().get();
        Double sum = 0.0;

        for (Comment comment : place.getComments()) {
            sum += comment.getRating();
        }
        return place.getComments().size() != 0 ? sum / place.getComments().size() : 5.0;
    }
}