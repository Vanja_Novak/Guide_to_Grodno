package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.UserProfile;
import edu.grsu.guide.support.dao.UserProfileDao;
import edu.grsu.guide.support.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:35
 */

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    private final UserProfileDao userProfileDao;

    @Autowired
    public UserProfileServiceImpl(UserProfileDao userProfileDao) {
        this.userProfileDao = userProfileDao;
    }

    public UserProfile findById(int id) {
        return userProfileDao.findById(id);
    }

    public UserProfile findByType(String type) {
        return userProfileDao.findByType(type);
    }

    public List<UserProfile> findAll() {
        return userProfileDao.findAll();
    }
}