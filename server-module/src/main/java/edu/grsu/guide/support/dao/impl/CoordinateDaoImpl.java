package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.Coordinates;
import edu.grsu.guide.support.dao.CoordinateDao;
import org.springframework.stereotype.Repository;

/**
 * @author Vanja Novak
 * @version 1.0 11.03.2017 3:50
 */
@Repository("coordinateDao")
public class CoordinateDaoImpl extends AbstractDaoImpl<Integer, Coordinates> implements CoordinateDao {
}
