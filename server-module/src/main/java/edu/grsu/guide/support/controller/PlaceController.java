package edu.grsu.guide.support.controller;

import edu.grsu.guide.domain.Coordinates;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.support.service.CommentService;
import edu.grsu.guide.support.service.PlaceGroupService;
import edu.grsu.guide.support.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;

/**
 * Контроллер для Меток.
 * <p>
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 17:38
 */
@Controller
@RequestMapping(value = "/api/place")
public class PlaceController extends BasicController {

    @Autowired
    private PlaceService placeService;

    @Autowired
    private PlaceGroupService placeGroupService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> getPlaceByParams(
            @RequestParam(value = "placeGroupCode", required = false) String placeGroupCode,
            @RequestParam(value = "ratingPlace", required = false) Integer ratingPlace) {
        return buildSuccess(placeService.getPlaceByParam(placeGroupCode, ratingPlace));
    }

    @RequestMapping(value = "/{placeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> getPlaceById(
            @PathVariable("placeId") Integer entityId) {
        return buildSuccess(placeService.getById(entityId));
    }

    @RequestMapping(value = "/groups/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> getAllPlaceGroups() {
        return buildSuccess(placeGroupService.getAll());
    }

    /**
     * @param entityId
     * @return
     */
    @RequestMapping(value = "/comments/{placeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> getPlaceComments(
            @PathVariable("placeId") Integer entityId) {
        return buildSuccess(commentService.getCommentsForPlace(entityId));
    }

    /**
     * Метод добавления комменатрия к сущности
     *
     * @param message
     * @param entityId
     * @return
     */
    @RequestMapping(value = "/comments/{entityId}/{message}/{rating}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> createCommentForPlace(
            @PathVariable("entityId") Integer entityId,
            @PathVariable("message") String message,
            @PathVariable("rating") Integer rating) throws UnsupportedEncodingException {

        if (rating != null && rating > 5) {
            rating = 0;
        }
        commentService.createCommentForPlace(entityId, message, rating);
        return buildResponse(null, HttpStatus.OK, null);
    }


    @RequestMapping(value = "/createPlace", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<?> createPlace(
            @RequestParam("title") String title,
            @RequestParam("name") String name,
            @RequestParam("groups") String groups,
            @RequestParam("description") String description,
            @RequestParam("latitude") Double latitude,
            @RequestParam("longitude") Double longitude) {

        Place place = new Place();
        place.setTitle(title);
        place.setName(name);
        place.setGroup(placeGroupService.getByCode(groups));
        place.setDescription(description);
        place.setCoordinate(new Coordinates(latitude, longitude));
        place.setModeration(false);

        placeService.updateOrCreatePlace(place);

        return buildResponse(null, HttpStatus.OK, null);
    }


    @RequestMapping(value = "/history", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    ResponseEntity<?> getHistoryPlace(
            @RequestParam(value = "nextId", required = false) Integer nextId) {
        return buildSuccess(placeService.getHistoryPlace(nextId));
    }

}
