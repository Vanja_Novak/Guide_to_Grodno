package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.Route;
import edu.grsu.guide.domain.dto.RoutesShortDto;

import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 2:30
 */
public interface RouteDao extends AbstractDao<Route> {

    List<RoutesShortDto> getAllShortRoute();
}