package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.BaseEntity;

import java.util.List;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 21:25
 */
public interface AbstractService<T extends BaseEntity, DTO_T> {

    /**
     * @return список всех меток на карте.
     */
    List<T> getAll();

    /**
     * @param id идентификатор объекта.
     * @return выбранная сущность.
     */
    DTO_T getById(Integer id);
}
