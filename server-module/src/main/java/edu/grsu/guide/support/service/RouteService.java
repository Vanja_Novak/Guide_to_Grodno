package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.Route;
import edu.grsu.guide.domain.dto.RouteDto;
import edu.grsu.guide.domain.dto.RouteTypeDto;
import edu.grsu.guide.domain.dto.RoutesShortDto;

import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 2:29
 */
public interface RouteService extends AbstractService<Route, RouteDto> {

    Route getRouteForUpdate(Integer id);

    void updateOrCreateRoute(Route route);

    List<RouteTypeDto> getAllRouteTypes();

    List<RoutesShortDto> getAllShortRoute();
}