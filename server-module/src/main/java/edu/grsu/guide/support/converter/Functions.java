package edu.grsu.guide.support.converter;

import edu.grsu.guide.domain.Place;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 12:08
 */
public class Functions {

    private Functions() {
    }

    public static boolean contains(Collection<Place> collection, Integer itemId) {

        List<Place> places = new ArrayList<>(collection);

        for (Place place : places) {
            if (place.getId().equals(itemId)) {
                return true;
            }
        }

        return false;
    }
}