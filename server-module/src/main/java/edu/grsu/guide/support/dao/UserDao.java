package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.User;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:36
 */
public interface UserDao {

    User findById(int id);

    User findBySSO(String sso);

    void save(User user);

    void deleteBySSO(String sso);

    List<User> findAllUsers();
}