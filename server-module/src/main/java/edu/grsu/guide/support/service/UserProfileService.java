package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.UserProfile;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:35
 */
public interface UserProfileService {

    UserProfile findById(int id);

    UserProfile findByType(String type);

    List<UserProfile> findAll();

}