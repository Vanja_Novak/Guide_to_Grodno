package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.dto.*;

import java.util.List;
import java.util.Set;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 18:23
 */
public interface PlaceService extends AbstractService<Place, PlaceDto> {

    List<Place> getPlaceByParam(String placeGroupCode, Integer ratingPlace);

    List<Place> getPlacesFullField();

    Place getPlaceForUpdate(Integer id);

    Place getByCoordinate(Double latitude, Double longitude);

    boolean existPlaceForCoordinate(Double latitude, Double longitude);

    void bindImagesForPlace(Place place, Set<DataStoreImageDto> images);

    void updateOrCreatePlace(Place place);

    HistoryDto getHistoryPlace(Integer id);

    ResponseGameDto entrancePointToCircle(Integer placeId, Double latitude, Double longitude);

    GameDto game();

    void removePlace(Integer placeId);

}
