package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.UserProfile;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:36
 */
public interface UserProfileDao {

    List<UserProfile> findAll();

    UserProfile findByType(String type);

    UserProfile findById(int id);
}