package edu.grsu.guide.support.controller;

import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.Route;
import edu.grsu.guide.domain.UserProfile;
import edu.grsu.guide.support.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 11:56
 */
@Controller
@RequestMapping("/admin")
@SessionAttributes("roles")
public class AdministrationController extends BasicController {

    @Autowired
    PlaceService placeService;

    @Autowired
    RouteService routeService;

    @Autowired
    ImageService imageService;

    @Autowired
    private PlaceGroupService placeGroupService;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;

    @Autowired
    AuthenticationTrustResolver authenticationTrustResolver;

    @Autowired
    DropBoxService dropBoxService;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        if (isCurrentAuthenticationAnonymous()) {
            return "login";
        } else {
            return "redirect:/admin/panel/";
        }
    }


    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            persistentTokenBasedRememberMeServices.logout(request, response, auth);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return "redirect:/admin/login?logout";
    }

    @RequestMapping(value = "/panel", method = RequestMethod.GET)
    public String listPlaces(ModelMap model) {
        List<Place> placeList = placeService.getAll();
        model.addAttribute("places", placeList);
        model.addAttribute("loggedinuser", getPrincipal());
        return "panelPlace";
    }

    @RequestMapping(value = {"/edit-place-{placeId}"}, method = RequestMethod.GET)
    public String editPlace(@PathVariable Integer placeId, ModelMap model) {
        Place place = placeService.getPlaceForUpdate(placeId);
        model.addAttribute("place", place);
        model.addAttribute("edit", true);
        model.addAttribute("groups", placeGroupService.getAll());
        model.addAttribute("loggedinuser", getPrincipal());
        return "placeCard";
    }

    @RequestMapping(value = {"/remove-place-{placeId}"}, method = RequestMethod.GET)
    public String removePlace(@PathVariable Integer placeId, ModelMap model) {
        placeService.removePlace(placeId);
        model.addAttribute("loggedinuser", getPrincipal());
        return "redirect:/admin/panel";
    }

    @RequestMapping(value = {"/create-pace"}, method = RequestMethod.GET)
    public String createPlace(ModelMap model) {
        model.addAttribute("place", new Place());
        model.addAttribute("edit", false);
        model.addAttribute("groups", placeGroupService.getAll());
        model.addAttribute("loggedinuser", getPrincipal());
        return "placeCard";
    }

    @RequestMapping(value = {"/create-pace"}, method = RequestMethod.POST)
    public String savePlace(@Valid Place place, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "placeCard";
        }
        placeService.updateOrCreatePlace(place);

        model.addAttribute("loggedinuser", getPrincipal());
        return "redirect:/admin/panel/";
    }


    @RequestMapping(value = {"/edit-place-{placeId}"}, method = RequestMethod.POST)
    public String updatePlace(@Valid Place place, BindingResult result,
                              ModelMap model, @PathVariable Integer placeId) {

        if (result.hasErrors()) {
            return "placeCard";
        }
        placeService.updateOrCreatePlace(place);
        model.addAttribute("loggedinuser", getPrincipal());
        return "redirect:/admin/panel/";
    }

    @RequestMapping(value = "/panel-admin-places", method = RequestMethod.GET)
    public String panaelAdminPlaces(ModelMap model) {

        model.addAttribute("images", imageService.getAll());
        model.addAttribute("loggedinuser", getPrincipal());
        return "panelAdminPlaces";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage(ModelMap model) {
        model.addAttribute("loggedinuser", getPrincipal());
        return "accessDenied";
    }

    //region: DropBox Service
    @RequestMapping(value = {"/update-image-base"}, method = RequestMethod.GET)
    public String updateImageBase(ModelMap model) {
        dropBoxService.synchronization();
        model.addAttribute("success", true);
        return "panelAdminPlaces";
    }
    //endregion


    //region: Route
    @RequestMapping(value = "/panel-routing", method = RequestMethod.GET)
    public String routeList(ModelMap model) {
        List<Route> routeList = routeService.getAll();
        model.addAttribute("routes", routeList);
        model.addAttribute("loggedinuser", getPrincipal());
        return "panelRouting";
    }

    @RequestMapping(value = {"/edit-route-{routeId}"}, method = RequestMethod.GET)
    public String editRoute(@PathVariable Integer routeId, ModelMap model) {
        Route route = routeService.getRouteForUpdate(routeId);
        model.addAttribute("route", route);
        model.addAttribute("places", placeService.getAll());
        model.addAttribute("edit", true);
        model.addAttribute("loggedinuser", getPrincipal());
        return "routeCard";
    }

    @RequestMapping(value = {"/edit-route-{routeId}"}, method = RequestMethod.POST)
    public String editRoute(@Valid Route route, BindingResult result,
                            ModelMap model, @PathVariable Integer routeId) {

        if (result.hasErrors()) {
            return "routeCard";
        }

        routeService.updateOrCreateRoute(route);
        model.addAttribute("loggedinuser", getPrincipal());
        return "redirect:/admin/panel/";
    }

    @RequestMapping(value = {"/create-route"}, method = RequestMethod.GET)
    public String createRoute(ModelMap model) {
        model.addAttribute("route", new Route());
        model.addAttribute("places", placeService.getAll());
        model.addAttribute("edit", false);
        model.addAttribute("groups", placeGroupService.getAll());
        model.addAttribute("loggedinuser", getPrincipal());
        return "routeCard";
    }

    @RequestMapping(value = {"/create-route"}, method = RequestMethod.POST)
    public String saveRoute(@Valid Route route, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "placeCard";
        }

        routeService.updateOrCreateRoute(route);
        model.addAttribute("loggedinuser", getPrincipal());
        return "redirect:/admin/panel/";
    }
    //endregion

    @ModelAttribute("roles")
    public List<UserProfile> initializeProfiles() {
        return userProfileService.findAll();
    }

    private String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails) principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }
}