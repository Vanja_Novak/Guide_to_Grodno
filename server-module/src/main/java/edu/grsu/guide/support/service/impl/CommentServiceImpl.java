package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.Comment;
import edu.grsu.guide.domain.dto.CommentDto;
import edu.grsu.guide.support.dao.CommentDao;
import edu.grsu.guide.support.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 19:09
 */
@Service("commentService")
public class CommentServiceImpl extends BasicService<Comment, CommentDto> implements CommentService {

    public static final String DTO_COMMENT = "commentDto";

    @Autowired
    private CommentDao commentDao;

    @Override
    public List<CommentDto> getCommentsForPlace(Integer placeId) {
        return convertToList(new ArrayList<>(commentDao.getCommentsForPlace(placeId)),
                CommentDto.class, DTO_COMMENT);
    }

    @Override
    public void createCommentForPlace(Integer placeId, String message, Integer rating) {
        commentDao.createCommentForPlace(placeId, message, rating);
    }
}