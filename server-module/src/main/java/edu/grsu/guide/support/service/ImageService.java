package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.dto.ImageDto;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 21:25
 */
public interface ImageService extends AbstractService<Image, ImageDto> {

    void saveImage(Image image);

    void deleteImages();
}