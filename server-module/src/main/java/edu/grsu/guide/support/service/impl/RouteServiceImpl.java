package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.Route;
import edu.grsu.guide.domain.RouteType;
import edu.grsu.guide.domain.dto.RouteDto;
import edu.grsu.guide.domain.dto.RouteTypeDto;
import edu.grsu.guide.domain.dto.RoutesShortDto;
import edu.grsu.guide.support.converter.CustomConverter;
import edu.grsu.guide.support.dao.RouteDao;
import edu.grsu.guide.support.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 2:30
 */
@Service("routeService")
public class RouteServiceImpl extends BasicService<Route, RouteDto> implements RouteService {

    public static final String DTO_ASS_ROUTE = "route";

    @Autowired
    private RouteDao routeDao;

    @Override
    public List<Route> getAll() {
        return convertToList(routeDao.getAll(), Route.class, DTO_ASS_ROUTE);
    }

    @Override
    public RouteDto getById(Integer id) {
        return convert(routeDao.getById(id), RouteDto.class, DTO_ASS_ROUTE);
    }

    @Override
    public Route getRouteForUpdate(Integer id) {
        return routeDao.getById(id);
    }

    @Override
    public void updateOrCreateRoute(Route routeModify) {

        Route route;
        if (routeModify.getId() != null) {
            route = routeDao.getById(routeModify.getId());
            route.setTitle(CustomConverter.getEncodingText(routeModify.getTitle()));
            route.setDescription(CustomConverter.getEncodingText(routeModify.getDescription()));
            route.setModeration(routeModify.getModeration());
            route.setPlaces(routeModify.getPlaces());

        } else {
            route = new Route();
            route.setTitle(CustomConverter.getEncodingText(routeModify.getTitle()));
            route.setDescription(CustomConverter.getEncodingText(routeModify.getDescription()));
            route.setModeration(routeModify.getModeration());
            route.setPlaces(routeModify.getPlaces());
        }

        routeDao.saveOrUpdate(route);

    }

    @Override
    public List<RouteTypeDto> getAllRouteTypes() {
        List<RouteTypeDto> dtos = new ArrayList<>();

        for (RouteType routeType : Arrays.asList(RouteType.values())) {
            dtos.add(new RouteTypeDto(routeType.name(), routeType.getTitle()));
        }

        return dtos;
    }

    @Override
    public List<RoutesShortDto> getAllShortRoute() {
        return routeDao.getAllShortRoute();
    }
}