package edu.grsu.guide.support.converter;

import edu.grsu.guide.domain.PlaceGroups;
import edu.grsu.guide.support.service.PlaceGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * @author Vanja Novak
 * @version 1.0 11.03.2017 2:10
 */
@Component
public class PlaceGroupToConverter implements Converter<String, PlaceGroups> {

    @Autowired
    private PlaceGroupService placeGroupService;

    @Override
    public PlaceGroups convert(String groupCode) {
        return placeGroupService.getByCode(groupCode);
    }
}
