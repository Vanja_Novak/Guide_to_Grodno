package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.support.dao.ImageDao;
import edu.grsu.guide.support.dao.PlaceDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Set;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 21:25
 */
@Repository("imageDao")
public class ImageDaoImpl extends AbstractDaoImpl<Integer, Image> implements ImageDao {

    @Autowired
    private PlaceDao placeDao;

    @Override
    @Transactional
    public void deleteImages() {
        getSession().createSQLQuery("truncate table Image").executeUpdate();
    }

    @Override
    @Transactional
    public List<Image> getImagesForPlace(Place place) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("place.id", place.getId()));

        return criteria.list();
    }
}