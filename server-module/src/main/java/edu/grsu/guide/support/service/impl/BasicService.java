package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.BaseEntity;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Базовые методы для своя сервисов.
 * <p>
 * User: Vanja Novak
 * Date : 11.02.2017
 * Time : 13:17
 */
@SuppressWarnings("unchecked")
public abstract class BasicService<ENT_T extends BaseEntity, DTO_T> {

    @Autowired
    private Mapper dtoMapper;

    /**
     * Конвертирование объекта в Dto.
     *
     * @param <T>         the generic type
     * @param from        the from
     * @param to          the to
     * @param mappingCase the mapping case
     * @return the t
     */
    protected <T> T convert(Object from, Class<T> to, String... mappingCase) {
        if (to != null && from != null) {
            if (mappingCase != null && mappingCase.length > 0) {
                return dtoMapper.map(from, to, mappingCase[0]);
            } else {
                return dtoMapper.map(from, to);
            }
        }
        return null;
    }


    /**
     * Конвертирование списка объектов в список Dto.
     *
     * @param <T>         the generic type
     * @param fromList    the from list
     * @param to          the to
     * @param mappingCase the mapping case
     * @return the list
     */
    protected <T> List<T> convertToList(List<?> fromList, Class<T> to, String... mappingCase) {
        List<T> result = new ArrayList<T>();
        for (Object from : fromList) {
            result.add(convert(from, to, mappingCase));
        }
        return result;
    }
}