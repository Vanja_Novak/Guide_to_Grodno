package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.Comment;
import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.ImageType;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.dto.GameDto;
import edu.grsu.guide.domain.dto.HistoryDto;
import edu.grsu.guide.domain.dto.PlaceDto;
import edu.grsu.guide.domain.dto.ResponseGameDto;
import edu.grsu.guide.support.dao.CommentDao;
import edu.grsu.guide.support.dao.ImageDao;
import edu.grsu.guide.support.dao.PlaceDao;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 17:22
 */
@Repository("placeDao")
public class PlaceDaoImpl extends AbstractDaoImpl<Integer, Place> implements PlaceDao {

    @Autowired
    private ImageDao imageDao;
    @Autowired
    private CommentDao commentDao;

    @Override
    @Transactional
    public List<PlaceDto> getPlaceByParam(String placeGroupCode, Integer ratingPlace) {
        Criteria criteria = createEntityCriteria();

        if (placeGroupCode != null && !placeGroupCode.isEmpty()) {
            criteria.createAlias("group", "group_alias");
            criteria.add(Restrictions.eq("group_alias.code", placeGroupCode));
        }

        if (ratingPlace != null) {
            //TODO -- после доработки БД опередлить поиск по рейтингу метки
        }

        //Проверяем, что бы на UI Отдавались только провалидированные метки
        criteria.add(Restrictions.eq("moderation", true));

        return criteria.list();
    }

    @Override
    @Transactional
    public Place getById(Integer id) {
        Place place = super.getById(id);
        place.setImages(imageDao.getImagesForPlace(place));

        return place;
    }

    @Override
    public Place getByCoordinate(Double latitude, Double longitude) {
        Criteria criteria = createEntityCriteria();

        criteria.createAlias("coordinate", "coordinate_alias");
        criteria.add(
                Restrictions.and(
                        Restrictions.eq("coordinate_alias.latitude", latitude),
                        Restrictions.eq("coordinate_alias.longitude", longitude)
                )
        );

        return (Place) criteria.list().get(0);
    }

    @Override
    public boolean existPlaceForCoordinate(Double latitude, Double longitude) {
        Criteria criteria = createEntityCriteria();
        criteria.createAlias("coordinate", "coordinate_alias");
        criteria.add(
                Restrictions.and(
                        Restrictions.eq("coordinate_alias.latitude", latitude),
                        Restrictions.eq("coordinate_alias.longitude", longitude)
                )
        );

        List entry = criteria.list();

        return entry != null && !entry.isEmpty();
    }

    @Override
    @Transactional
    public HistoryDto getHistoryPlace(Integer id) {

        HistoryDto historyDto = new HistoryDto();

        Criteria palceCriteria = createEntityCriteria();
        palceCriteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));
        palceCriteria.setMaxResults(1);

        Place place = (Place) palceCriteria.uniqueResult();

        Criteria imageCrit = getSession().createCriteria(Image.class);
        imageCrit.add(Restrictions.eq("place.id", place.getId()));

        List<Image> images = imageCrit.list();

        Collections.shuffle(images);
        Optional<Image> newImgUrl = images.stream().filter(item -> item.getImageType().name().equals(ImageType.NEW.name())).findFirst();
        Optional<Image> oldImgUrl = images.stream().filter(item -> item.getImageType().name().equals(ImageType.OLD.name())).findFirst();

        historyDto.setDescription(place.getDescription());
        historyDto.setOldPhotoUrl(oldImgUrl.map(Image::getFileUrl).orElse(null));
        historyDto.setNewPhotoUrl(newImgUrl.map(Image::getFileUrl).orElse(null));

        return historyDto;
    }

    @Override
    @Transactional
    public ResponseGameDto entrancePointToCircle(Integer placeId, Double latitude, Double longitude) {

        //http://127.0.0.1:8080/api/place/entrancePointToCircle?placeId=55&latitude=23.846922&longitude=53.689195
        /*
        SELECT id, ( 6371 * acos( cos( radians(23.84692) ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE )
            - radians(53.689195) ) + sin( radians(23.84692) ) * sin( radians( LATITUDE ) ) ) )
            AS distance FROM Coordinates HAVING distance < 1 ORDER BY distance ;
         */
        ResponseGameDto responseGameDto = new ResponseGameDto();

        String sql = "SELECT id, ( 6371 * acos( cos( radians(" + latitude + ") ) * cos( radians( LATITUDE ) ) * cos( radians( LONGITUDE )  - radians(" + longitude + ") ) + sin( radians(" + latitude + ") ) * sin( radians( LATITUDE ) ) ) ) AS distance FROM Coordinates HAVING distance < 1 ORDER BY distance ";
        SQLQuery query = getSession().createSQLQuery(sql);
        query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);


        List results = query.list();
        Place place = getById(placeId);

        for (Object object : results) {
            Map row = (Map) object;
            Integer id = (Integer) row.get("id");

            if (id.equals(place.getCoordinate().getId())) {
                responseGameDto.setHasGot(true);
                Double distance = (Double) row.get("distance");
                responseGameDto.setMeters((int) (distance * 1000));
                responseGameDto.setDescription(place.getDescription());
            }
        }

        if (!responseGameDto.isHasGot()) {
            //При первом разе, если не указали верное добавляем описание места.
            responseGameDto.setDescription(place.getDescription());
        }

        return responseGameDto;
    }

    @Override
    @Transactional
    public GameDto game() {
        GameDto gameDto = new GameDto();

        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.sqlRestriction("1=1 order by rand()"));
        criteria.setMaxResults(1);
        Place place = (Place) criteria.uniqueResult();

        Criteria imageCrit = getSession().createCriteria(Image.class);
        imageCrit.add(Restrictions.eq("place.id", place.getId()));
        imageCrit.setMaxResults(4);

        List<Image> images = imageCrit.list();

        if (images.isEmpty()) {
            //Если по выбранной точке нет фото - попробовать еще раз
            return game();
        }

        for (Image image : images) {
            gameDto.getPhotos().add(image.getFileUrl());
        }
        gameDto.setPlaceId(place.getId());

        return gameDto;
    }

    @Override
    @Transactional
    public void removePlace(Integer placeId) {
        Place place = getById(placeId);

        for (Image image : place.getImages()) {
            imageDao.delete(image);
        }

        for (Comment comment : place.getComments()) {
            commentDao.delete(comment);
        }
        getSession().createSQLQuery("DELETE FROM Route_Place where PLACE_ID=" + place.getId()).executeUpdate();

        delete(place);
    }

    @Override
    @Transactional
    public List<Place> getAll() {
        Criteria c = createEntityCriteria();
        c.setProjection(Projections.projectionList()
                .add(Projections.groupProperty("id").as("id"))
                .add(Projections.property("id").as("id"))
                .add(Projections.property("title").as("title"))
                .add(Projections.property("description").as("description"))
                .add(Projections.property("moderation").as("moderation"))
                .add(Projections.property("updateDate").as("updateDate"))
                .add(Projections.property("group").as("group"))
                .add(Projections.property("coordinate").as("coordinate"))
                .add(Projections.property("comments").as("comments"))
                .add(Projections.property("name").as("name")));
        c.setResultTransformer(Transformers.aliasToBean(Place.class));
        return (List<Place>) c.list();
    }
}
