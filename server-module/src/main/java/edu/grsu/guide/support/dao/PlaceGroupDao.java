package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.PlaceGroups;

/**
 * User: Vanja Novak
 * Date : 23.02.2017
 * Time : 13:08
 */
public interface PlaceGroupDao extends AbstractDao<PlaceGroups> {

    PlaceGroups getByCode(String code);
}
