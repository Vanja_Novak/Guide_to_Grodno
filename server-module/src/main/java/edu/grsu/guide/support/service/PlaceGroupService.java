package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.PlaceGroups;
import edu.grsu.guide.domain.dto.PlaceGroupShortDto;

/**
 * User: Vanja Novak
 * Date : 23.02.2017
 * Time : 13:06
 */
public interface PlaceGroupService extends AbstractService<PlaceGroups, PlaceGroupShortDto> {

    PlaceGroups getByCode(String code);

}
