package edu.grsu.guide.support.service.impl;

import edu.grsu.guide.domain.PlaceGroups;
import edu.grsu.guide.domain.dto.PlaceGroupShortDto;
import edu.grsu.guide.support.dao.PlaceGroupDao;
import edu.grsu.guide.support.service.PlaceGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 23.02.2017
 * Time : 13:06
 */
@Service("placeGroupService")
public class PlaceGroupServiceImpl extends BasicService<PlaceGroups, PlaceGroupShortDto> implements PlaceGroupService {

    public static final String DTO_PLACE_GROUP_SHORT = "placeGroupShort";

    @Autowired
    private PlaceGroupDao placeGroupDao;

    public List<PlaceGroups> getAll() {
        return convertToList(placeGroupDao.getAll(), PlaceGroups.class, DTO_PLACE_GROUP_SHORT);
    }

    public PlaceGroupShortDto getById(Integer id) {
        return convert(placeGroupDao.getById(id), PlaceGroupShortDto.class, DTO_PLACE_GROUP_SHORT);
    }

    @Override
    public PlaceGroups getByCode(String code) {
        return placeGroupDao.getByCode(code);
    }
}
