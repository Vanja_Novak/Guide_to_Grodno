package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.Coordinates;

/**
 * @author Vanja Novak
 * @version 1.0 11.03.2017 3:49
 */
public interface CoordinateDao extends AbstractDao<Coordinates> {
}
