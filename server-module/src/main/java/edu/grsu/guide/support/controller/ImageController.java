package edu.grsu.guide.support.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 20:06
 */
@Controller
@RequestMapping(value = "/api/image")
public class ImageController extends BasicController {

}