package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.Route;
import edu.grsu.guide.domain.dto.RoutesShortDto;
import edu.grsu.guide.support.dao.RouteDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 2:31
 */
@Repository("routeDao")
public class RouteDaoImpl extends AbstractDaoImpl<Integer, Route> implements RouteDao {

    @Override
    @Transactional
    public List<Route> getAll() {
        Criteria criteria = createEntityCriteria();
        //TODO
       // criteria.add(Restrictions.eq("moderation", true));

        return criteria.list();
    }

    @Override
    @Transactional
    public List<RoutesShortDto> getAllShortRoute() {
        List<RoutesShortDto> res = new ArrayList<>();

        List<Route> routeList = createEntityCriteria().list();

        for (Route route : routeList) {
            RoutesShortDto dto = new RoutesShortDto();
            dto.setId(route.getId());
            dto.setTitle(route.getTitle());
            dto.setDescription(route.getDescription());
            dto.setUpdateDate(route.getUpdateDate());
            dto.getCountPlaces(route.getPlaces().size());

            res.add(dto);
        }

        return res;
    }
}