package edu.grsu.guide.support.dao.impl;

import edu.grsu.guide.domain.PlaceGroups;
import edu.grsu.guide.support.dao.PlaceGroupDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * User: Vanja Novak
 * Date : 23.02.2017
 * Time : 13:09
 */
@Repository("placeGroupDao")
public class PlaceGroupDaoImpl extends AbstractDaoImpl<Integer, PlaceGroups> implements PlaceGroupDao {

    @Override
    @Transactional
    public PlaceGroups getByCode(String code) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("code", code));
        criteria.setMaxResults(1);
        return (PlaceGroups) criteria.uniqueResult();
    }
}
