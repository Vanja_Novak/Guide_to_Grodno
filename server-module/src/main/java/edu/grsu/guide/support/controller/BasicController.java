package edu.grsu.guide.support.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * Created by Vanja Novak
 * Date : 09.10.2016
 * Time : 0:44
 */
public class BasicController {

    private static final Logger log = LoggerFactory.getLogger(BasicController.class);

    /**
     * Build success response entity.
     *
     * @param <T>  the type parameter
     * @param body the body
     * @return the response entity
     */
    protected <T> ResponseEntity<T> buildSuccess(T body) {
        return buildResponse(body, HttpStatus.OK, null);
    }


    /**
     * Build response response entity.
     *
     * @param <T>        the type parameter
     * @param body       the body
     * @param httpStatus the http status
     * @return the response entity
     */
    protected <T> ResponseEntity<T> buildResponse(T body, HttpStatus httpStatus) {
        return buildResponse(body, httpStatus, null);
    }

    /**
     * Build response response entity.
     *
     * @param <T>        the type parameter
     * @param body       the body
     * @param httpStatus the http status
     * @param mediaType  the media type
     * @return the response entity
     */
    protected <T> ResponseEntity<T> buildResponse(T body, HttpStatus httpStatus, MediaType mediaType) {
        if (httpStatus == null) {
            throw new NullPointerException();
        }

        HttpHeaders headers;

        if (mediaType != null) {
            headers = new HttpHeaders();
            if (MediaType.TEXT_HTML.equals(mediaType)) {
                headers.add("Content-Type", "text/html; charset=utf-8");
            } else {
                headers.setContentType(mediaType);
            }
        } else {
            headers = null;
        }

        return new ResponseEntity<T>(body, headers, httpStatus);
    }
}