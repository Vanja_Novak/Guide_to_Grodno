package edu.grsu.guide.support.controller;

import edu.grsu.guide.support.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author Vanja Novak
 * @version 1.0 23.04.2017 3:01
 */
@Controller
@RequestMapping(value = "/api/route")
public class RouteController extends BasicController {

    @Autowired
    private RouteService routeService;

    @RequestMapping(value = "/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<?> getAllShortRoute() {
        return buildSuccess(routeService.getAllShortRoute());
    }

    @RequestMapping(value = "/{routeId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<?> getRouteId(@PathVariable("routeId") Integer routeId) {
        return buildSuccess(routeService.getById(routeId));
    }


    @RequestMapping(value = "/routeType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<?> getAllRouteTypes() {
        return buildSuccess(routeService.getAllRouteTypes());
    }
}