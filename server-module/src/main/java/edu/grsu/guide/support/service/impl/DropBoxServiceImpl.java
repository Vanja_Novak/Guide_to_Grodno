package edu.grsu.guide.support.service.impl;

import com.dropbox.core.*;
import edu.grsu.guide.domain.ImageType;
import edu.grsu.guide.domain.Place;
import edu.grsu.guide.domain.dto.CoordinateDto;
import edu.grsu.guide.domain.dto.DataStoreImageDto;
import edu.grsu.guide.support.service.DropBoxService;
import edu.grsu.guide.support.service.ImageService;
import edu.grsu.guide.support.service.PlaceService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author Vanja Novak
 * @version 1.0 25.03.2017 14:41
 */
@Service("dropBoxService")
@PropertySource(value = {"classpath:application.properties"})
public class DropBoxServiceImpl implements DropBoxService {

    final static Logger logger = Logger.getLogger(DropBoxServiceImpl.class);

    private final String IMAGE_ROOT = "/images";
    private final String FOLDER_NEW_IMAGE_PREFIX = "/new";
    private final String FOLDER_OLD_IMAGE_PREFIX = "/old";

    @Autowired
    private Environment environment;
    @Autowired
    private PlaceService placeService;
    @Autowired
    private ImageService imageService;

    Map<CoordinateDto, String> dropBoxActualFolder;
    List<Place> placesList;

    private DbxClient getClient() {
        DbxRequestConfig reqConfig = new DbxRequestConfig(environment.getRequiredProperty("dropbox.clientIdentifier"),
                Locale.getDefault().toString());
        return new DbxClient(reqConfig, environment.getRequiredProperty("dropbox.accessToken"));
    }

    @Override
    @Transactional
    public void synchronization() {
        DbxClient client = getClient();
        try {
            DbxEntry.WithChildren root = client.getMetadataWithChildren(IMAGE_ROOT);
            placesList = placeService.getPlacesFullField();
            dropBoxActualFolder = new HashMap<>();
            syncFolder(client, root);
            syncData(client, root);
        } catch (DbxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Синхронизовать каталог папок имеющихся на диске.
     *
     * @param client клиент подключения к облаку.
     * @param root   корневой каталог.
     */
    private void syncFolder(DbxClient client, DbxEntry.WithChildren root) {
        //Создаем HashMap со всеми папками имеющимися в DropBox
        for (DbxEntry dbxEntry : root.children) {
            String folderName = dbxEntry.name;
            String[] coordinates = folderName.replaceAll("(^.*?\\[|\\]\\s*$)", "").split(",");
            Double latitude = Double.parseDouble(coordinates[1]);
            Double longitude = Double.parseDouble(coordinates[0]);

            dropBoxActualFolder.put(new CoordinateDto(latitude, longitude), dbxEntry.path);
        }

        //Подчищаем данные в dropBox
        dropBoxActualFolder.forEach((k, v) -> {
            if (!existForPlaces(k.getLatitude(), k.getLongitude())) {
                try {
                    client.delete(v);
                    logger.debug(String.format("В Бд не найден объект с координатами %s,%s. Запись о данном объекте будет удалена из DropBox", k.getLatitude(), k.getLongitude()));
                } catch (DbxException e) {
                    e.printStackTrace();
                }
            }
        });

        placesList.forEach(item -> {
            if (item.getCoordinate() != null) {
                String nameFolder = IMAGE_ROOT + "/" + item.getTitle().replace("\"", "") + " " + "[" + item.getCoordinate().getLongitude() + "," + item.getCoordinate().getLatitude() + "]";
                try {
                    if (getPathFolder(item.getCoordinate().getLatitude(), item.getCoordinate().getLongitude()) == null) {
                        client.createFolder(nameFolder);
                        client.createFolder(nameFolder + FOLDER_OLD_IMAGE_PREFIX);
                        client.createFolder(nameFolder + FOLDER_NEW_IMAGE_PREFIX);
                    }
                } catch (DbxException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Обновить ссылки на фото хранимые в БД.
     *
     * @param client клиент подключения к облаку.
     * @param root   корневой каталог.
     */
    private void syncData(DbxClient client, DbxEntry.WithChildren root) throws DbxException {

        imageService.deleteImages();

        for (DbxEntry child : root.children) {
            if (child.isFolder()) {
                logger.debug("Название папки: " + child.name);

                String[] coordinates = child.name.replaceAll("(^.*?\\[|\\]\\s*$)", "").split(",");
                Double latitude = Double.parseDouble(coordinates[1]);
                Double longitude = Double.parseDouble(coordinates[0]);

                logger.debug("Координаты объекта: " + latitude + "," + longitude);

                Place place = getPlaceForCoordinate(latitude, longitude);
                Set<DataStoreImageDto> imageDtoSet = new HashSet<>();
                if (place != null) {
                    logger.debug("Объект с данными параметрами присуствует в БД : " + place.getId());

                    DbxEntry.WithChildren folderNewImages = client.getMetadataWithChildren(child.path + FOLDER_NEW_IMAGE_PREFIX);
                    for (DbxEntry itemNewImage : folderNewImages.children) {
                        String path = client.createShareableUrl(itemNewImage.path).replace("dl=0", "raw=1");
                        imageDtoSet.add(new DataStoreImageDto(path, ImageType.NEW));
                    }

                    DbxEntry.WithChildren folderOldImages = client.getMetadataWithChildren(child.path + FOLDER_OLD_IMAGE_PREFIX);
                    for (DbxEntry itemOldImage : folderOldImages.children) {
                        String path = client.createShareableUrl(itemOldImage.path).replace("dl=0", "raw=1");
                        imageDtoSet.add(new DataStoreImageDto(path, ImageType.OLD));
                    }
                    placeService.bindImagesForPlace(place, imageDtoSet);
                }
            }
        }
    }

    private String getPathFolder(Double lat, Double lot) {
        final String[] path = new String[1];
        dropBoxActualFolder.forEach((k, v) -> {
            if (k.getLatitude().equals(lat) && k.getLongitude().equals(lot)) {
                path[0] = v;
            }
        });

        return path[0];
    }

    private boolean existForPlaces(Double lat, Double lon) {
        return placesList.stream().filter(item -> item.getCoordinate().getLatitude().equals(lat) && item.getCoordinate().getLongitude().equals(lon)).count() != 0;
    }

    private Place getPlaceForCoordinate(Double lat, Double lon) {
        final Place[] place = {null};
        placesList.forEach(item -> {
            if (item.getCoordinate().getLongitude().equals(lon) && item.getCoordinate().getLatitude().equals(lat)) {
                place[0] = item;
            }
        });
        return place[0];
    }

}
