package edu.grsu.guide.support.service;

import edu.grsu.guide.domain.User;

import java.util.List;

/**
 * User: Vanja Novak
 * Date : 18.02.2017
 * Time : 12:34
 */
public interface UserService {

    User findById(int id);

    User findBySSO(String sso);

    void saveUser(User user);

    void updateUser(User user);

    void deleteUserBySSO(String sso);

    List<User> findAllUsers();

    boolean isUserSSOUnique(Integer id, String sso);

}