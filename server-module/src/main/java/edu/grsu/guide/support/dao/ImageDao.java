package edu.grsu.guide.support.dao;


import edu.grsu.guide.domain.Image;
import edu.grsu.guide.domain.Place;

import java.util.List;

/**
 * @author Vanja Novak
 * @version 1.0 05.03.2017 21:25
 */
public interface ImageDao extends AbstractDao<Image> {

    void deleteImages();

    List<Image> getImagesForPlace(Place place);
}