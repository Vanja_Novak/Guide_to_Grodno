package edu.grsu.guide.support.dao;

import edu.grsu.guide.domain.BaseEntity;

import java.util.List;

/**
 * User : Vanja Novak
 * Date : 05.02.2017
 * Time : 17:20
 */
public interface AbstractDao<T extends BaseEntity> {

    void saveOrUpdate(T entity);

    void delete(Integer id);

    void delete(T entity);

    T getById(Integer id);

    List<T> getAll();
}
